<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/migration/library/ConstMigration.php');
include($strRootPath . '/src/migration/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/exception/MigrationCollectionInvalidFormatException.php');
include($strRootPath . '/src/migration/exception/GetConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/exception/ExecuteEnableCallException.php');
include($strRootPath . '/src/migration/exception/RollbackEnableCallException.php');
include($strRootPath . '/src/migration/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/migration/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/migration/api/MigrationInterface.php');
include($strRootPath . '/src/migration/api/MigrationCollectionInterface.php');
include($strRootPath . '/src/migration/model/DefaultMigration.php');
include($strRootPath . '/src/migration/model/DefaultMigrationCollection.php');

include($strRootPath . '/src/migration/version/library/ConstVersionMigration.php');
include($strRootPath . '/src/migration/version/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/version/exception/GetConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/version/model/VersionMigration.php');

include($strRootPath . '/src/migration/version/fix/library/ConstFixVersionMigration.php');
include($strRootPath . '/src/migration/version/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/version/fix/model/FixVersionMigration.php');

include($strRootPath . '/src/migration/factory/library/ConstMigrationFactory.php');
include($strRootPath . '/src/migration/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/migration/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/migration/factory/api/MigrationFactoryInterface.php');
include($strRootPath . '/src/migration/factory/model/DefaultMigrationFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/build/directory/library/ConstDirBuilder.php');
include($strRootPath . '/src/build/directory/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/directory/model/DirBuilder.php');

include($strRootPath . '/src/migrator/library/ConstMigrator.php');
include($strRootPath . '/src/migrator/exception/MigrationCollectionInvalidFormatException.php');
include($strRootPath . '/src/migrator/exception/MigrationNotFoundException.php');
include($strRootPath . '/src/migrator/api/MigratorInterface.php');
include($strRootPath . '/src/migrator/model/DefaultMigrator.php');