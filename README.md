LibertyCode_Migration
=====================



Description
-----------

Library contains migration components, 
to manage application migrations.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/migration ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/migration": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Migration

Migrations allow to to execute and rollback scripts, 
in specific order,
from specific configuration.

_Elements_

- Migration

    Allows to design a migration, 
    who is an item containing all information, 
    to execute and rollback script.
    in precise sorting position.

- VersionMigration

    Extends migration features. 
    Uses standard versions and order positions, 
    to sort migrations.

- FixVersionMigration

    Extends version migration features. 
    Allows to execute and rollback script,
    from a specified fixed configuration.
    
- MigrationCollection

    Allows to design collection of migrations.
    Allows to get sorting list of migrations.
    
- MigrationFactory

    Allows to design a migration factory, 
    to provide new or specified migration instance,
    from specified configuration or file path.
    
_Example_

```php
// Get migration factory
use liberty_code\migration\migration\factory\model\DefaultMigrationFactory;
$migrationFactory = new DefaultMigrationFactory();
...
// Get new migration from configuration
$migration = $migrationFactory->getObjMigration(...);
...
// Get new migration from migration class file path
$migration = $migrationFactory->getObjMigrationFromFile(...);
...
```

#### Builder

Builder allows to hydrate migration collection, with migrations.

_Elements_

- Builder
    
    Uses array of source data to hydrate migration collection.

- DirBuilder
    
    Extends builder features. 
    Uses array of directories paths, 
    to hydrate migration collection,
    from each file path, on each directory path.
    
_Example_

```php
// Get migration collection
use liberty_code\migration\migration\model\DefaultMigrationCollection;
$migrationCollection = new DefaultMigrationCollection();
...
// Get migration builder
use liberty_code\migration\build\model\DefaultBuilder;
$migrationBuilder = new DefaultBuilder($migrationFactory);
...
// Hydrate migration collection
$migrationBuilder->setTabDataSrc(array(...));
$migrationBuilder->hydrateMigrationCollection($migrationCollection);
...
// Get migration directory builder
use liberty_code\migration\build\directory\model\DirBuilder;
$migrationDirBuilder = new DirBuilder($migrationFactory);
...
// Hydrate migration collection
$migrationDirBuilder->setTabDataSrc(array(...));
$migrationDirBuilder->hydrateMigrationCollection($migrationCollection, false);
...
foreach($migrationCollection->getTabKey() as $key) {
    echo($migrationCollection->getObjMigration($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Migration key 1
 * ...
 * Migration key N
 */
...
```

#### Migrator

Migrator allows to execute and rollback migrations, 
using migration collection.

```php
use liberty_code\migration\migrator\model\DefaultMigrator;
$migrator = new DefaultMigrator($migrationCollection);
...
// Execute migrations not already executed
$migrator->execute();
...
// Rollback migrations already executed
$migrator->rollback();
...
// Refresh migrations: 
// - Rollback migrations already executed
// - Execute all migrations after
$migrator->refresh();
...
```

---


