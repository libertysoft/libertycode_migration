<?php
/**
 * Description :
 * This class allows to describe behavior of migrator class.
 * Migrator allows to execute and rollback migrations,
 * using migration collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migrator\api;

use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\api\MigrationCollectionInterface;



interface MigratorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified migration key is found.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);





	// Methods getters
	// ******************************************************************************

	/**
	 * Get migration collection object.
	 *
	 * @return MigrationCollectionInterface
	 */
	public function getObjMigrationCollection();



    /**
     * Get migration from specified key.
     *
     * @param string $strKey
     * @return MigrationInterface
     */
    public function getObjMigration($strKey);





	// Methods setters
	// ******************************************************************************

    /**
     * Set migration collection object.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     */
    public function setMigrationCollection(MigrationCollectionInterface $objMigrationCollection);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified migration.
     *
     * @param string $strKey
     * @param boolean $boolForce = false : Allows to force access in case migration already executed.
     */
    public function executeMigration($strKey, $boolForce = false);



    /**
     * Execute migrations.
     *
     * Configuration format: @see MigrationCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @param null|boolean $isExecuted = false
     */
    public function execute(array $tabConfig = null, $isExecuted = false);



    /**
     * Rollback specified migration.
     *
     * @param string $strKey
     * @param boolean $boolForce = false : Allows to force access in case migration not already executed.
     */
    public function rollbackMigration($strKey, $boolForce = false);



    /**
     * Rollback migrations.
     *
     * Configuration format: @see MigrationCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @param null|boolean $isExecuted = true
     */
    public function rollback(array $tabConfig = null, $isExecuted = true);



    /**
     * Refresh specified migration:
     * Rollback and execute migration.
     *
     * @param string $strKey
     * @param boolean $boolForce
     */
    public function refreshMigration($strKey, $boolForce = false);



    /**
     * Refresh migrations:
     * Rollback and execute migrations.
     *
     * Configuration format: @see MigrationCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @param null|boolean $rollbackIsExecuted = true
     * @param null|boolean $executeIsExecuted = false
     */
    public function refresh(
        array $tabConfig = null,
        $rollbackIsExecuted = true,
        $executeIsExecuted = false
    );
}