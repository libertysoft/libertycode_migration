<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migrator\exception;

use liberty_code\migration\migrator\library\ConstMigrator;



class MigrationNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $key
     */
	public function __construct($key)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMigrator::EXCEPT_MSG_MIGRATION_NOT_FOUND,
            mb_strimwidth(strval($key), 0, 50, "...")
        );
	}
}