<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migrator\exception;

use liberty_code\migration\migrator\library\ConstMigrator;
use liberty_code\migration\migration\api\MigrationCollectionInterface;



class MigrationCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $migrationCollection
     */
	public function __construct($migrationCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMigrator::EXCEPT_MSG_MIGRATION_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($migrationCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified migration collection has valid format.
	 * 
     * @param mixed $migrationCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($migrationCollection)
    {
		// Init var
		$result = (
			(is_null($migrationCollection)) ||
			($migrationCollection instanceof MigrationCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($migrationCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}