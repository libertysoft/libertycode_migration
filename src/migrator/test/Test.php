<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/migrator/test/MigratorTest.php');



// Init var
$tabDataSrc = array(
    $strRootAppPath . '/src/migration/test/migration'
);

$objMigrationDirBuilder->setTabDataSrc($tabDataSrc);
$objMigrationDirBuilder->hydrateMigrationCollection($objMigrationCollection);

$objMigrator->getObjMigration('TestMigration2')->setIsExecuted(true);



// Test execute/rollback migration
$tabInfo = array(
    [
        'TestMigration1Update',
        false
    ],
    // Execute ok, rollback ok, refresh ko (rollback failed)

    [
        'TestMigration2',
        false
    ],
    // Execute ko (already executed)

    [
        'TestMigration2',
        true
    ],
    //  Execute ok, rollback ok, refresh ok

    [
        'TestMigration2Update',
        true
    ]
    //  Ko: migration not found
);

foreach($tabInfo as $info)
{
    echo('Test execute/rollback migration: <br />');
    echo('Info: <pre>');var_dump($info);echo('</pre>');

	try{
        $strKey = $info[0];
        $boolForce = (isset($info[1]) ? $info[1] : false);

        $objMigrator->executeMigration($strKey, $boolForce);
        $objMigrator->rollbackMigration($strKey, $boolForce);
        $objMigrator->refreshMigration($strKey, $boolForce);

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test execute
$objMigrator->getObjMigration('TestMigration2')->setIsExecuted(false);
$objMigrator->getObjMigration('TestMigration7')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration8')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration9')->setIsExecuted(true);

echo('Test execute: <br />');
$objMigrator->execute();

echo('<br /><br /><br />');

$objMigrator->getObjMigration('TestMigration7')->setIsExecuted(false);
$objMigrator->getObjMigration('TestMigration8')->setIsExecuted(false);
$objMigrator->getObjMigration('TestMigration9')->setIsExecuted(false);

echo('Test execute (Already executed): <br />');
$objMigrator->execute(null, true);

echo('<br /><br /><br />');



// Test rollback
$objMigrator->getObjMigration('TestMigration7')->setIsExecuted(false);
$objMigrator->getObjMigration('TestMigration8')->setIsExecuted(false);
$objMigrator->getObjMigration('TestMigration9')->setIsExecuted(false);

echo('Test rollback: <br />');
$objMigrator->rollback();

echo('<br /><br /><br />');

$objMigrator->getObjMigration('TestMigration7')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration8')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration9')->setIsExecuted(true);

echo('Test rollback (Not already executed): <br />');
$objMigrator->rollback(null, false);

echo('<br /><br /><br />');



// Test refresh
$objMigrator->getObjMigration('TestMigration4')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration5')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration6')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration7')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration8')->setIsExecuted(true);
$objMigrator->getObjMigration('TestMigration9')->setIsExecuted(true);

echo('Test refresh: <br />');
$objMigrator->refresh();

echo('<br /><br /><br />');

echo('Test refresh (with selection): <br />');
$objMigrator->refresh(
    [
        'sort_compare_key' => [
            [
                'operator' => 'greater_equal',
                'value' => 'TestMigration2'
            ],
            [
                'operator' => 'less_equal',
                'value' => 'TestMigration7'
            ]
        ]
    ]
);

echo('<br /><br /><br />');


