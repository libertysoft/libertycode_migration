<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/MigrationBuilderTest.php');

// Use
use liberty_code\migration\migration\model\DefaultMigrationCollection;
use liberty_code\migration\migrator\model\DefaultMigrator;



// Init var
$objMigrationCollection = new DefaultMigrationCollection();
$objMigrator = new DefaultMigrator($objMigrationCollection);


