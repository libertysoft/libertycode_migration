<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migrator\library;



class ConstMigrator
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_MIGRATION_COLLECTION = 'objMigrationCollection';



    // Exception message constants
    const EXCEPT_MSG_MIGRATION_COLLECTION_INVALID_FORMAT =
        'Following migration collection "%1$s" invalid! 
        It must be a migration collection object.';
    const EXCEPT_MSG_MIGRATION_NOT_FOUND = 'No migration found from following key "%1s"!';
}