<?php
/**
 * Description :
 * This class allows to define default migrator class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migrator\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\migration\migrator\api\MigratorInterface;

use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migrator\library\ConstMigrator;
use liberty_code\migration\migrator\exception\MigrationCollectionInvalidFormatException;
use liberty_code\migration\migrator\exception\MigrationNotFoundException;



class DefaultMigrator extends FixBean implements MigratorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigrationCollectionInterface $objMigrationCollection
     */
    public function __construct(MigrationCollectionInterface $objMigrationCollection)
    {
        // Call parent constructor
        parent::__construct();

        // Init migration collection
        $this->setMigrationCollection($objMigrationCollection);
    }



	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION))
        {
            $this->__beanTabData[ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION] = null;
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION:
                    MigrationCollectionInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return $this->getObjMigrationCollection()->checkExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjMigrationCollection()
    {
        // Return result
        return $this->beanGet(ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION);
    }



    /**
     * @inheritdoc
     * @throws MigrationNotFoundException
     */
    public function getObjMigration($strKey)
    {
        // Set check argument
        if(!$this->checkExists($strKey))
        {
            throw new MigrationNotFoundException($strKey);
        }

        // Return result
        return $this->getObjMigrationCollection()->getObjMigration($strKey);
    }

	



    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setMigrationCollection(MigrationCollectionInterface $objMigrationCollection)
	{
		$this->beanSet(ConstMigrator::DATA_KEY_DEFAULT_MIGRATION_COLLECTION, $objMigrationCollection);
	}


	

	
	// Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws MigrationNotFoundException
     */
    public function executeMigration($strKey, $boolForce = false)
    {
        $this->getObjMigration($strKey)->execute($boolForce);
    }



    /**
     * @inheritdoc
     */
    public function execute(array $tabConfig = null, $isExecuted = false)
    {
        // Init var
        $objMigrationCollection = $this->getObjMigrationCollection();
        $tabKey = $objMigrationCollection->getTabSortKey($tabConfig, true, $isExecuted);

        // Init force option (if selection of potential migrations, already executed)
        $boolForce = ((!is_bool($isExecuted)) || $isExecuted);

        // Run each selected migration
        foreach($tabKey as $strKey)
        {
            // Execute migration
            $objMigration = $objMigrationCollection->getObjMigration($strKey);
            $objMigration->execute($boolForce);
        }
    }



    /**
     * @inheritdoc
     * @throws MigrationNotFoundException
     */
    public function rollbackMigration($strKey, $boolForce = false)
    {
        $this->getObjMigration($strKey)->rollback($boolForce);
    }



    /**
     * @inheritdoc
     */
    public function rollback(array $tabConfig = null, $isExecuted = true)
    {
        // Init var
        $objMigrationCollection = $this->getObjMigrationCollection();
        $tabKey = $objMigrationCollection->getTabSortKey($tabConfig, false, $isExecuted);

        // Init force option (if selection of potential migrations, not already executed)
        $boolForce = ((!is_bool($isExecuted)) || (!$isExecuted));

        // Run each selected migration
        foreach($tabKey as $strKey)
        {
            // Rollback migration
            $objMigration = $objMigrationCollection->getObjMigration($strKey);
            $objMigration->rollback($boolForce);
        }
    }



    /**
     * @inheritdoc
     */
    public function refreshMigration($strKey, $boolForce = false)
    {
        $this->rollbackMigration($strKey, $boolForce);
        $this->executeMigration($strKey);
    }



    /**
     * @inheritdoc
     */
    public function refresh(
        array $tabConfig = null,
        $rollbackIsExecuted = true,
        $executeIsExecuted = false
    )
    {
        $this->rollback($tabConfig, $rollbackIsExecuted);
        $this->execute($tabConfig, $executeIsExecuted);
    }
	
	
	
}