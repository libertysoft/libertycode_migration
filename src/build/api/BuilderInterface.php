<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified migration collection instance, with migrations.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\build\api;

use liberty_code\migration\migration\api\MigrationCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified migration collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @param boolean $boolClear = true
     */
    public function hydrateMigrationCollection(MigrationCollectionInterface $objMigrationCollection, $boolClear = true);
}