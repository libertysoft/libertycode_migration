<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\build\directory\exception;

use liberty_code\migration\build\directory\library\ConstDirBuilder;



class DataSrcInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
     */
	public function __construct($dataSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstDirBuilder::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
			mb_strimwidth(strval($dataSrc), 0, 10, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified data source has valid format.
     *
     * @param mixed $dataSrc
     * @return boolean
     */
    protected static function checkDataSrcIsValid($dataSrc)
    {
        $result =
            // Check valid array
            is_array($dataSrc) &&

            // Check each data is unique
            (count(array_unique($dataSrc)) == count($dataSrc));

        // Check each data is valid
        $dataSrc = array_values($dataSrc);
        for($intCpt = 0; ($intCpt < count($dataSrc)) && $result; $intCpt++)
        {
            $result =
                // Check valid directory path
                is_string($dataSrc[$intCpt]) &&
                (trim($dataSrc[$intCpt]) != '') &&
                file_exists($dataSrc[$intCpt]) &&
                is_dir($dataSrc[$intCpt]) &&
                is_readable($dataSrc[$intCpt]);
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified data source has valid format.
	 * 
     * @param mixed $dataSrc
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($dataSrc)
    {
		// Init var
		$result =
            // Check valid data source
            static::checkDataSrcIsValid($dataSrc);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc));
		}
		
		// Return result
		return $result;
    }
	
	
	
}