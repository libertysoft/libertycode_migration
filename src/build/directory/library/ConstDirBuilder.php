<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\build\directory\library;



class ConstDirBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be an array and following the directory migration collection builder data source standard.';
}