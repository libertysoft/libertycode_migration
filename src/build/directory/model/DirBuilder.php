<?php
/**
 * Description :
 * This class allows to define directory builder class.
 * Directory builder allows to populate migration collection,
 * from a specified array of source data.
 *
 * Directory builder uses the following specified source data, to hydrate migration collection:
 * [
 *     "String unique migration directory path 1:
 *      Each php file considered as migration class"
 *
 *     ...,
 *
 *     "String unique migration directory path N"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\build\directory\model;

use liberty_code\migration\build\model\DefaultBuilder;

use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\build\library\ConstBuilder;
use liberty_code\migration\build\directory\exception\DataSrcInvalidFormatException;



class DirBuilder extends DefaultBuilder
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateMigrationCollection(MigrationCollectionInterface $objMigrationCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabMigration = array();
        foreach($tabDataSrc as $strDirPath)
        {
            $tabFilePath = glob($strDirPath . '/*.php');

            // Run all files
            foreach($tabFilePath as $strFilePath)
            {
                // Check valid file path
                if(is_file($strFilePath) && is_readable($strFilePath))
                {
                    // Get new migration
                    $objMigration = $objFactory->getObjMigrationFromFile($strFilePath);

                    // Register migration, if found
                    if(!is_null($objMigration))
                    {
                        $tabMigration[] = $objMigration;
                    }
                    // Throw exception if migration not found, from data source
                    else
                    {
                        throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
                    }
                }
            }
        }

        // Clear migrations from collection, if required
        if($boolClear)
        {
            $objMigrationCollection->removeMigrationAll();
        }

        // Register migrations on collection
        $objMigrationCollection->setTabMigration($tabMigration);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
}