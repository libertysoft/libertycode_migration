<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate migration collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate migration collection:
 * [
 *     // Migration configuration 1
 *     Migration factory configuration (@see MigrationFactoryInterface ),
 *
 *     ...,
 *
 *     // Migration configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\migration\build\api\BuilderInterface;

use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migration\factory\api\MigrationFactoryInterface;
use liberty_code\migration\build\library\ConstBuilder;
use liberty_code\migration\build\exception\FactoryInvalidFormatException;
use liberty_code\migration\build\exception\DataSrcInvalidFormatException;



/**
 * @method null|MigrationFactoryInterface getObjFactory() Get migration factory object.
 * @method void setObjFactory(null|MigrationFactoryInterface $objFactory) Set migration factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigrationFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        MigrationFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init migration factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_FACTORY, null);
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC, array());
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateMigrationCollection(MigrationCollectionInterface $objMigrationCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabMigration = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new migration
            $key = (is_string($key) ? $key : null);
            $objMigration = $objFactory->getObjMigration($tabConfig, $key);

            // Register migration, if found
            if(!is_null($objMigration))
            {
                $tabMigration[] = $objMigration;
            }
            // Throw exception if migration not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear migrations from collection, if required
        if($boolClear)
        {
            $objMigrationCollection->removeMigrationAll();
        }

        // Register migrations on collection
        $objMigrationCollection->setTabMigration($tabMigration);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
}