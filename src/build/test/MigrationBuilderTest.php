<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/migration/factory/test/MigrationFactoryTest.php');

// Use
use liberty_code\migration\build\model\DefaultBuilder;
use liberty_code\migration\build\directory\model\DirBuilder;

$objMigrationBuilder = new DefaultBuilder(
    $objMigrationFactory
);

$objMigrationDirBuilder = new DirBuilder(
    $objMigrationFactory
);


