<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/MigrationBuilderTest.php');
require_once($strRootAppPath . '/src/migration/test/TestMigration.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration1.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration2.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration3.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration4.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration5.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration6.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration7.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration8.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration9.php');

// Use
use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\model\DefaultMigrationCollection;
use liberty_code\migration\migration\test\migration\TestMigration1;
use liberty_code\migration\migration\test\migration\TestMigration2;
use liberty_code\migration\migration\test\migration\TestMigration3;
use liberty_code\migration\migration\test\migration\TestMigration4;
use liberty_code\migration\migration\test\migration\TestMigration5;
use liberty_code\migration\migration\test\migration\TestMigration6;
use liberty_code\migration\migration\test\migration\TestMigration7;
use liberty_code\migration\migration\test\migration\TestMigration8;
use liberty_code\migration\migration\test\migration\TestMigration9;



// Init var
$objMigration1 = new TestMigration1();
$objMigration2 = new TestMigration2();
$objMigration3 = new TestMigration3();
$objMigration4 = new TestMigration4();
$objMigration5 = new TestMigration5();
$objMigration6 = new TestMigration6();
$objMigration7 = new TestMigration7();
$objMigration8 = new TestMigration8();
$objMigration9 = new TestMigration9();
$objMigrationCollection = new DefaultMigrationCollection();

$tabDataSrc = array(
    'TestMigration1Update2_not_care' =>  array_merge(
        $objMigration1->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration1.php',
            //'key' => 'TestMigration1Update2'
        ]
    ),
    'TestMigration2Update' => array_merge(
        $objMigration2->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration2.php',
            //'order' => 1
        ]
    ),
    array_merge(
        $objMigration3->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration3.php',
            //'version' => '1.0.1',
            'order' => 3
        ]
    ),
    array_merge(
        $objMigration4->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration4.php',
            'key' => 'TestMigration4Update',
            //'version' => '1.0.2'
        ]
    ),
    array_merge(
        $objMigration5->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration5.php',
            'key' => 'TestMigration5Update',
            //'version' => '1.0.2'
        ]
    ),
    array_merge(
        $objMigration6->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration6.php',
            //'version' => '1.0.2'
        ]
    ),
    [
        'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration7.php'
    ],
    [
        'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration8.php'
    ],
    [
        'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration9.php'
    ]
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objMigrationBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objMigrationBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objMigrationBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate migration collection
echo('Test hydrate migration collection: <br />');

$objMigrationBuilder->hydrateMigrationCollection($objMigrationCollection, false);
foreach($objMigrationCollection as $strKey => $objMigration)
{
    /** @var MigrationInterface $objMigration */
	try{
		echo('Migration "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objMigration));echo('</pre>');
        echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
        echo('Check: is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
        echo('Get: migration collection count: <pre>');
            print_r(count($objMigration->getObjMigrationCollection()->getTabKey()));
        echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test clear migration collection
echo('Test clear: <br />');

echo('Before clearing: <pre>');print_r($objMigrationCollection->getTabKey());echo('</pre>');

$objMigrationBuilder->setTabDataSrc(array());
$objMigrationBuilder->hydrateMigrationCollection($objMigrationCollection);
echo('After clearing: <pre>');print_r($objMigrationCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate migration collection (with directory)
$tabDataSrc = array(
    $strRootAppPath . '/src/migration/test/migration'
);

echo('Test hydrate migration collection (with directory): <br />');

$objMigrationDirBuilder->setTabDataSrc($tabDataSrc);
$objMigrationDirBuilder->hydrateMigrationCollection($objMigrationCollection, false);
foreach($objMigrationCollection as $strKey => $objMigration)
{
    /** @var MigrationInterface $objMigration */
    try{
        echo('Migration "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objMigration));echo('</pre>');
        echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
        echo('Check: is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
        echo('Get: migration collection count: <pre>');
        print_r(count($objMigration->getObjMigrationCollection()->getTabKey()));
        echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }

    echo('<br /><br />');
}

echo('<br /><br /><br />');


