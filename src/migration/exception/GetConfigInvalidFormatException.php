<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\exception;

use liberty_code\migration\migration\library\ConstMigration;



class GetConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstMigration::EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = is_string($strValue) && (trim($strValue) != '');
            }

            return $result;
        };

        // Init sort comparison on key index array check function
        $checkTabSortCompareKeyIsValid = function($tabSortCompareKey)
        {
            $result = is_array($tabSortCompareKey) && (count($tabSortCompareKey) > 0);

            // Run each sub-configuration
            $tabSortCompareKey = array_values($tabSortCompareKey);
            for($intCpt = 0; ($intCpt < count($tabSortCompareKey)) && $result; $intCpt++)
            {
                $subConfig = $tabSortCompareKey[$intCpt];
                $result =
                    // Check valid operator
                    (
                        (!isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR])) ||
                        (
                            is_string($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR]) &&
                            in_array(
                                $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR],
                                ConstMigration::getTabGetConfigSortCompareOperator()
                            )
                        )
                    ) &&

                    // Check valid value
                    isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) &&
                    is_string($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) &&
                    (trim($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) != '');
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid regexp key
            (
                (!isset($config[ConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY])) ||
                (
                    is_string($config[ConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY]) &&
                    (trim($config[ConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY]) != '')
                )
            ) &&

            // Check valid list key
            (
                (!isset($config[ConstMigration::TAB_GET_CONFIG_KEY_LIST_KEY])) ||
                $checkTabStrIsValid($config[ConstMigration::TAB_GET_CONFIG_KEY_LIST_KEY])
            ) &&

            // Check valid sort comparison on key
            (
                (!isset($config[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY])) ||
                $checkTabSortCompareKeyIsValid($config[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&

                // Check valid config
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}