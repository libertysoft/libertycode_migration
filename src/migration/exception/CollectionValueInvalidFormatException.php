<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\exception;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationInterface;



class CollectionValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $value
     */
	public function __construct($value) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstMigration::EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format.
	 * 
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($value)
    {
		// Init var
		$result = (
		    (!is_null($value)) &&
            ($value instanceof MigrationInterface)
        );
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}