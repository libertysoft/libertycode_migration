<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\exception;

use liberty_code\migration\migration\library\ConstMigration;



class ExecuteEnableCallException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMigration::EXCEPT_MSG_EXECUTE_ENABLE_CALL,
            mb_strimwidth(strval($strKey), 0, 50, "...")
        );
	}
}