<?php
/**
 * Description :
 * This class allows to describe behavior of migration factory class.
 * Migration factory allows to provide new or specified migration instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined migration types or file path.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\factory\api;

use liberty_code\migration\migration\api\MigrationInterface;



interface MigrationFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of migration,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrMigrationClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get string class path of migration,
     * from specified file path
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @return null|string
     */
    public function getStrMigrationClassPathFromFile($strFilePath, array $tabConfig = null);



    /**
     * Get new or specified object instance migration,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param MigrationInterface $objMigration = null
     * @return null|MigrationInterface
     */
    public function getObjMigration(
        array $tabConfig,
        $strConfigKey = null,
        MigrationInterface $objMigration = null
    );



    /**
     * Get new or specified object instance migration,
     * hydrated from specified file path,
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @param MigrationInterface $objMigration = null
     * @return null|MigrationInterface
     */
    public function getObjMigrationFromFile(
        $strFilePath,
        array $tabConfig = null,
        MigrationInterface $objMigration = null
    );
}