<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/migration/factory/test/MigrationFactoryTest.php');
require_once($strRootAppPath . '/src/migration/test/TestMigration.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration1.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration2.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration3.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration4.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration5.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration6.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration7.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration8.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration9.php');

// Use
use liberty_code\migration\migration\model\DefaultMigrationCollection;
use liberty_code\migration\migration\test\migration\TestMigration1;
use liberty_code\migration\migration\test\migration\TestMigration2;
use liberty_code\migration\migration\test\migration\TestMigration3;
use liberty_code\migration\migration\test\migration\TestMigration4;
use liberty_code\migration\migration\test\migration\TestMigration5;
use liberty_code\migration\migration\test\migration\TestMigration6;
use liberty_code\migration\migration\test\migration\TestMigration7;
use liberty_code\migration\migration\test\migration\TestMigration8;
use liberty_code\migration\migration\test\migration\TestMigration9;



// Init var
$objMigration1 = new TestMigration1();
$objMigration2 = new TestMigration2();
$objMigration3 = new TestMigration3();
$objMigration4 = new TestMigration4();
$objMigration5 = new TestMigration5();
$objMigration6 = new TestMigration6();
$objMigration7 = new TestMigration7();
$objMigration8 = new TestMigration8();
$objMigration9 = new TestMigration9();
$objMigrationCollection = new DefaultMigrationCollection();
$objMigrationFactory->setObjMigrationCollection($objMigrationCollection);



// Test new migration
$tabMigrationData = array(
    [
        array_merge(
            $objMigration1->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration1.php',
                'key' => 'TestMigration1Update2'
            ]
        ),
        'TestMigration1Update2_not_care',
        $objMigration2
    ], // Ko: not found: migration 2 used for migration 1 config

    [
        array_merge(
            $objMigration1->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration1.php',
                'key' => 'TestMigration1Update2',
                //'version' => '1.0.0',
                //'order' => 0
            ]
        ),
        'TestMigration1Update2_not_care',
        $objMigration1
    ], // Ok

    [
        array_merge(
            $objMigration2->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration2.php',
                'order' => 1
            ]
        ),
        'TestMigration2Update',
        $objMigration3
    ], // Ko: not found: migration 3 used for migration 2 config

    [
        array_merge(
            $objMigration2->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration2.php',
                'order' => 1
            ]
        ),
        'TestMigration2Update',
        $objMigration2
    ], // Ok

    [
        array_merge(
            $objMigration3->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration3.php',
                'version' => '1.0.1',
                'order' => 3
            ]
        ),
        null,
        $objMigration1
    ], // Ko: not found: migration 1 used for migration 3 config

    [
        array_merge(
            $objMigration3->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration3.php',
                'version' => '1.0.1',
                'order' => 3
            ]
        ),
        null,
        $objMigration3
    ], // Ok

    [
        array_merge(
            $objMigration4->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration4.php',
                'key' => 'TestMigration4Update',
                'version' => '1.0.2'
            ]
        )
    ], // Ok

    [
        array_merge(
            $objMigration5->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration5.php',
                'key' => 'TestMigration5Update',
                'version' => '1.0.2'
            ]
        ),
        'TestMigration5Update_not_care',
    ], // Ok

    [
        array_merge(
            $objMigration6->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration6.php',
                'version' => '1.0.2'
            ]
        ),
        'TestMigration6Update',
    ], // Ok

    [
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration7.php'
        ]
    ], // Ok

    [
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration8.php'
        ]
    ], // Ok

    [
        [
            'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration9.php'
        ]
    ] // Ok
);

foreach($tabMigrationData as $migrationData)
{
    echo('Test new migration: <br />');
    echo('<pre>');var_dump($migrationData);echo('</pre>');

    try{
        $tabConfig = $migrationData[0];
        $strConfigKey = (isset($migrationData[1]) ? $migrationData[1] : null);
        $objMigration = (isset($migrationData[2]) ? $migrationData[2] : null);
        $objMigration = $objMigrationFactory->getObjMigration($tabConfig, $strConfigKey, $objMigration);

        echo('Class path: <pre>');var_dump($objMigrationFactory->getStrMigrationClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objMigration))
        {
            echo('Migration class path: <pre>');var_dump(get_class($objMigration));echo('</pre>');
            echo('Migration config: <pre>');var_dump($objMigration->getTabConfig());echo('</pre>');
            echo('Migration key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
            echo('Migration is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
            echo('Migration collection class path: <pre>');
                print_r(get_class($objMigration->getObjMigrationCollection()));
            echo('</pre>');
        }
        else
        {
            echo('Migration not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test new migration from file
$tabMigrationData = array(
    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration1_not_care.php',
        array_merge(
            $objMigration1->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration1.php',
                'key' => 'TestMigration1Update2'
            ]
        ),
        $objMigration2
    ], // Ko: not found: migration 2 used for migration 1 config

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration1_not_care.php',
        array_merge(
            $objMigration1->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration1.php',
                'key' => 'TestMigration1Update2',
                //'version' => '1.0.0',
                //'order' => 0
            ]
        ),
        $objMigration1
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration2.php',
        array_merge(
            $objMigration2->getTabConfig(),
            [
                'order' => 1
            ]
        ),
        $objMigration3
    ], // Ko: not found: migration 3 used for migration 2 config

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration2.php',
        array_merge(
            $objMigration2->getTabConfig(),
            [
                'order' => 1
            ]
        ),
        $objMigration2
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration3.php',
        array_merge(
            $objMigration3->getTabConfig(),
            [
                'version' => '1.0.1',
                'order' => 3
            ]
        ),
        $objMigration1
    ], // Ko: not found: migration 1 used for migration 3 config

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration3.php',
        array_merge(
            $objMigration3->getTabConfig(),
            [
                'version' => '1.0.1',
                'order' => 3
            ]
        ),
        $objMigration3
    ], // Ok

    [
        null,
        array_merge(
            $objMigration4->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/migration/test/migration/TestMigration4.php',
                'key' => 'TestMigration4Update',
                'version' => '1.0.2'
            ]
        )
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration5.php',
        array_merge(
            $objMigration5->getTabConfig(),
            [
                'key' => 'TestMigration5Update',
                'version' => '1.0.2'
            ]
        )
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration6.php',
        array_merge(
            $objMigration6->getTabConfig(),
            [
                'version' => '1.0.2'
            ]
        )
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration7.php'
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration8.php'
    ], // Ok

    [
        $strRootAppPath . '/src/migration/test/migration/TestMigration9.php'
    ] // Ok
);

foreach($tabMigrationData as $migrationData)
{
    echo('Test new migration from file: <br />');
    echo('<pre>');var_dump($migrationData);echo('</pre>');

    try{
        $strFilePath = $migrationData[0];
        $tabConfig = (isset($migrationData[1]) ? $migrationData[1] : null);
        $objMigration = (isset($migrationData[2]) ? $migrationData[2] : null);
        $objMigration = $objMigrationFactory->getObjMigrationFromFile($strFilePath, $tabConfig, $objMigration);

        echo('Class path: <pre>');var_dump($objMigrationFactory->getStrMigrationClassPathFromFile($strFilePath, $tabConfig));echo('</pre>');

        if(!is_null($objMigration))
        {
            echo('Migration class path: <pre>');var_dump(get_class($objMigration));echo('</pre>');
            echo('Migration config: <pre>');var_dump($objMigration->getTabConfig());echo('</pre>');
            echo('Migration key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
            echo('Migration is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
            echo('Migration collection class path: <pre>');
            print_r(get_class($objMigration->getObjMigrationCollection()));
            echo('</pre>');
        }
        else
        {
            echo('Migration not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


