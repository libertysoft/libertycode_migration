<?php
/**
 * Description :
 * This class allows to define default migration factory class.
 * Can be consider is base of all migration factory type.
 *
 * Default migration factory uses the following specified configuration, to get and hydrate migration:
 * [
 *     file_path(optional): "file path to migration class",
 *
 *     OR
 *
 *     type(optional): "string constant to determine migration type",
 *
 *     ... specific migration configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\migration\migration\factory\api\MigrationFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\library\reflection\library\ToolBoxFileReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migration\exception\MigrationCollectionInvalidFormatException;
use liberty_code\migration\migration\factory\library\ConstMigrationFactory;
use liberty_code\migration\migration\factory\exception\FactoryInvalidFormatException;
use liberty_code\migration\migration\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|MigrationCollectionInterface getObjMigrationCollection() Get migration collection object.
 * @method void setObjMigrationCollection(null|MigrationCollectionInterface $objMigrationCollection) Set migration collection object.
 * @method null|MigrationFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|MigrationFactoryInterface $objFactory) Set parent factory object.
 */
class DefaultMigrationFactory extends DefaultFactory implements MigrationFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigrationCollectionInterface $objMigrationCollection = null
     * @param MigrationFactoryInterface $objFactory = null
     */
    public function __construct(
        MigrationCollectionInterface $objMigrationCollection = null,
        MigrationFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init migration collection
        $this->setObjMigrationCollection($objMigrationCollection);

        // Init route factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMigrationFactory::DATA_KEY_DEFAULT_MIGRATION_COLLECTION))
        {
            $this->__beanTabData[ConstMigrationFactory::DATA_KEY_DEFAULT_MIGRATION_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstMigrationFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstMigrationFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified migration.
     * Overwrite it to set specific call hydration.
     *
     * @param MigrationInterface $objMigration
     * @param array $tabConfigFormat
     */
    protected function hydrateMigration(MigrationInterface $objMigration, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstMigrationFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstMigrationFactory::TAB_CONFIG_KEY_TYPE]);
        }
        if(array_key_exists(ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH]);
        }

        // Hydrate migration
        $objMigrationCollection = $this->getObjMigrationCollection();
        if(!is_null($objMigrationCollection))
        {
            $objMigration->setMigrationCollection($objMigrationCollection);
        }

        if(count($tabConfigFormat) > 0)
        {
            $objMigration->setConfig($tabConfigFormat);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMigrationFactory::DATA_KEY_DEFAULT_MIGRATION_COLLECTION,
            ConstMigrationFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMigrationFactory::DATA_KEY_DEFAULT_MIGRATION_COLLECTION:
                    MigrationCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstMigrationFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified migration object.
     *
     * @param MigrationInterface $objMigration
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(MigrationInterface $objMigration, array $tabConfigFormat)
    {
        // Init var
        $strMigrationClassPath = $this->getStrMigrationClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strMigrationClassPath)) &&
            ($strMigrationClassPath == get_class($objMigration))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param string $strFilePath = null
     * @return array
     */
    protected function getTabConfigFormat(
        array $tabConfig,
        $strConfigKey = null,
        $strFilePath = null
    )
    {
        // Init var
        $result = $tabConfig;

        // Use configuration key, considered as migration key, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstMigration::TAB_CONFIG_KEY_KEY, $result))
        )
        {
            $result[ConstMigration::TAB_CONFIG_KEY_KEY] = $strConfigKey;
        }

        // Use file path, if required
        if(
            (!is_null($strFilePath)) &&
            (!array_key_exists(ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH, $result))
        )
        {
            $result[ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH] = $strFilePath;
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured file path,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigFilePath(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstMigrationFactory::TAB_CONFIG_KEY_FILE_PATH];
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstMigrationFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstMigrationFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of migration,
     * from specified configured file path.
     *
     * @param null|string $strConfigFilePath
     * @return null|string
     */
    protected function getStrMigrationClassPathFromFilePath($strConfigFilePath)
    {
        // Init var
        $result = null;
        $tabClassPath = array_merge(
            ToolBoxFileReflection::getTabClassPath($strConfigFilePath),
            ToolBoxFileReflection::getTabInterfacePath($strConfigFilePath)
        );

        // Get class path, if required (first class path found)
        if(
            isset($tabClassPath[0]) &&
            is_subclass_of($tabClassPath[0], MigrationInterface::class)
        )
        {
            $result = $tabClassPath[0];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of migration,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    protected function getStrMigrationClassPathFromType($strConfigType)
    {
        // Return result
        return null;
    }



    /**
     * Get string class path of migration engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrMigrationClassPathEngine(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $strConfigFilePath = $this->getStrConfigFilePath($tabConfigFormat);
        $strConfigType = $this->getStrConfigType($tabConfigFormat);

        // Get class path
        $result = $this->getStrMigrationClassPathFromFilePath($strConfigFilePath);
        if(is_null($result))
        {
            $result = $this->getStrMigrationClassPathFromType($strConfigType);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrMigrationClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrMigrationClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrMigrationClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrMigrationClassPathFromFile($strFilePath, array $tabConfig = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getStrMigrationClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrMigrationClassPathFromFile($strFilePath, $tabConfig);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance migration,
     * from specified class path.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strClassPath
     * @return null|MigrationInterface
     */
    protected function getObjMigrationNew($strClassPath)
    {
        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance migration engine.
     *
     * @param array $tabConfigFormat
     * @param MigrationInterface $objMigration = null
     * @return null|MigrationInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjMigrationEngine(
        array $tabConfigFormat,
        MigrationInterface $objMigration = null
    )
    {
        // Init var
        $result = null;
        $strClassPath = $this->getStrMigrationClassPathEngine($tabConfigFormat);
        $objMigration = (
            is_null($objMigration) ?
                $this->getObjMigrationNew($strClassPath) :
                $objMigration
        );

        // Get and hydrate migration, if required
        if(
            (!is_null($objMigration)) &&
            $this->checkConfigIsValid($objMigration, $tabConfigFormat)
        )
        {
            $this->hydrateMigration($objMigration, $tabConfigFormat);
            $result = $objMigration;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjMigration(
        array $tabConfig,
        $strConfigKey = null,
        MigrationInterface $objMigration = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjMigrationEngine($tabConfigFormat, $objMigration);

        // Get migration from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjMigration($tabConfig, $strConfigKey, $objMigration);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjMigrationFromFile(
        $strFilePath,
        array $tabConfig = null,
        MigrationInterface $objMigration = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getObjMigrationEngine($tabConfigFormat, $objMigration);

        // Get migration from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjMigrationFromFile($strFilePath, $tabConfig, $objMigration);
        }

        // Return result
        return $result;
    }



}