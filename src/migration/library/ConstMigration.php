<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\library;



class ConstMigration
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_MIGRATION_COLLECTION = 'objMigrationCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
	
	

    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_IS_EXECUTED = 'is_executed';

    // Configuration get
    const TAB_GET_CONFIG_KEY_REGEXP_KEY = 'regexp_key';
    const TAB_GET_CONFIG_KEY_LIST_KEY = 'list_key';
    const TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY = 'sort_compare_key';
    const TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR = 'operator';
    const TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE = 'value';

    // Configuration get sort comparison operator configuration
    const GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL = 'equal';
    const GET_CONFIG_SORT_COMPARE_OPERATOR_LESS = 'less';
    const GET_CONFIG_SORT_COMPARE_OPERATOR_LESS_EQUAL = 'less_equal';
    const GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER = 'greater';
    const GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER_EQUAL = 'greater_equal';

    // Sort comparison value configuration
    const SORT_COMPARE_LESS = -1;
    const SORT_COMPARE_EQUAL = 0;
    const SORT_COMPARE_GREATER = 1;
	
	
	
    // Exception message constants
    const EXCEPT_MSG_MIGRATION_COLLECTION_INVALID_FORMAT =
        'Following migration collection "%1$s" invalid! 
        It must be a migration collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default migration configuration standard.';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default migration get configuration standard.';
    const EXCEPT_MSG_EXECUTE_ENABLE_CALL =
        'Impossible to call execution from following migration "%1$s"! 
        Maybe this migration is already executed.';
    const EXCEPT_MSG_ROLLBACK_ENABLE_CALL =
        'Impossible to call rollback from following migration "%1$s"! 
        Maybe this migration is not executed.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a migration object in collection.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of get configuration sort comparison operator values.
     *
     * @return array
     */
    static public function getTabGetConfigSortCompareOperator()
    {
        // Init var
        $result = array(
            self::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL,
            self::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS,
            self::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS_EQUAL,
            self::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER,
            self::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER_EQUAL
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of sort comparison values.
     *
     * @return array
     */
    static public function getTabSortCompare()
    {
        // Init var
        $result = array(
            self::SORT_COMPARE_LESS,
            self::SORT_COMPARE_EQUAL,
            self::SORT_COMPARE_GREATER
        );

        // Return result
        return $result;
    }



}