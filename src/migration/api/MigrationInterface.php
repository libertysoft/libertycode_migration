<?php
/**
 * Description :
 * This class allows to describe behavior of migration class.
 * Migration is item, containing all information to execute migration and rollback it,
 * in precise sorting position.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\api;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationCollectionInterface;



interface MigrationInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if migration is already executed.
     *
     * @return boolean
     */
    public function checkIsExecuted();



    /**
     * Check if migration is selected,
     * from the specified selection configuration.
     *
     * @param array $tabConfig
     * @return boolean
     */
    public function checkIsSelected(array $tabConfig);





	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get migration collection object.
	 *
	 * @return null|MigrationCollectionInterface
	 */
	public function getObjMigrationCollection();



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



	/**
	 * Get string key (considered as migration id).
	 *
	 * @return string
	 */
	public function getStrKey();



    /**
     * Get sort comparison analysis,
     * from the specified migration.
     * Result format:
     * @see ConstMigration::SORT_COMPARE_LESS if this migration less than specified migration.
     * @see ConstMigration::SORT_COMPARE_EQUAL if this migration equal specified migration.
     * @see ConstMigration::SORT_COMPARE_GREATER if this migration greater specified migration.
     *
     * @param $objMigration
     * @return integer
     */
    public function getIntSortCompare(MigrationInterface $objMigration);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set migration collection object.
	 * 
	 * @param MigrationCollectionInterface $objMigrationCollection = null
	 */
	public function setMigrationCollection(MigrationCollectionInterface $objMigrationCollection = null);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



    /**
     * Set check if migration is already executed.
     *
     * @param boolean $boolIsExecuted
     */
    public function setIsExecuted($boolIsExecuted);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute migration.
     *
     * @param boolean $boolForce = false : Allows to force access in case migration already executed.
     */
    public function execute($boolForce = false);



    /**
     * Rollback migration.
     *
     * @param boolean $boolForce = false : Allows to force access in case migration not already executed.
     */
    public function rollback($boolForce = false);
}