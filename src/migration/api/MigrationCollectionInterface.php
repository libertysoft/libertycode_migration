<?php
/**
 * Description :
 * This class allows to describe behavior of migration collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\api;

use liberty_code\migration\migration\api\MigrationInterface;



interface MigrationCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
     * Check if specified migration key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of keys.
	 *
     * Configuration format:
     * Selection configuration can be provided.
     * Null means no specific selection required and all keys will be selected.
     *
     * @param array $tabConfig = null
     * @param null|boolean $isExecuted = null
	 * @return array
	 */
	public function getTabKey(array $tabConfig = null, $isExecuted = null);



    /**
     * Get sorted index array of keys.
     * It sort ascending if sort asc is true, sort descending else.
     * On sorting equity, migration order kept.
     *
     * Configuration format: @see MigrationCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @param boolean $boolSortAsc = true
     * @param null|boolean $isExecuted = null
     * @return array
     */
    public function getTabSortKey(
        array $tabConfig = null,
        $boolSortAsc = true,
        $isExecuted = null
    );



	/**
	 * Get migration from specified key.
	 * 
	 * @param string $strKey
	 * @return null|MigrationInterface
	 */
	public function getObjMigration($strKey);
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set migration and return its key.
	 * 
	 * @param MigrationInterface $objMigration
	 * @return string
     */
	public function setMigration(MigrationInterface $objMigration);



    /**
     * Set list of migrations (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|MigrationCollectionInterface $tabMigration
     * @return array
     */
    public function setTabMigration($tabMigration);



    /**
     * Remove migration and return its instance.
     *
     * @param string $strKey
     * @return MigrationInterface
     */
    public function removeMigration($strKey);



    /**
     * Remove all migrations.
     *
     * Configuration format: @see MigrationCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @param null|boolean $isExecuted = null
     */
    public function removeMigrationAll(array $tabConfig = null, $isExecuted = null);
}