<?php
/**
 * Description :
 * This class allows to define default migration collection class.
 * key: migration key => migration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\migration\migration\api\MigrationCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\exception\CollectionKeyInvalidFormatException;
use liberty_code\migration\migration\exception\CollectionValueInvalidFormatException;



class DefaultMigrationCollection extends DefaultBean implements MigrationCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var MigrationInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjMigration($strKey)));
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Configuration format: @see MigrationInterface::checkIsSelected()
     *
     * @inheritdoc
     */
    public function getTabKey(array $tabConfig = null, $isExecuted = null)
    {
        // Init var
        $result = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
        $isExecuted = ((is_bool($isExecuted) || is_null($isExecuted)) ? $isExecuted : null);

        // Apply selection, if required
        if(is_array($tabConfig) || is_bool($isExecuted))
        {
            // Init var
            $result = array();
            $tabKey = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);

            // Run each migration
            foreach($tabKey as $strKey)
            {
                // Register migration, if required
                $objMigration = $this->getObjMigration($strKey);
                $boolMigrationIsExecuted = $objMigration->checkIsExecuted();
                if(
                    // Check selection, if required
                    (
                        (!is_array($tabConfig)) ||
                        $objMigration->checkIsSelected($tabConfig)
                    ) &&
                    // Check is executed, if required
                    (
                        (!is_bool($isExecuted)) ||
                        ($isExecuted && $boolMigrationIsExecuted) ||
                        ((!$isExecuted) && (!$boolMigrationIsExecuted))
                    )
                )
                {
                    $result[] = $strKey;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabSortKey(
        array $tabConfig = null,
        $boolSortAsc = true,
        $isExecuted = null
    )
    {
        // Init var
        $result = $this->getTabKey($tabConfig, $isExecuted);
        $boolSortAsc = (is_bool($boolSortAsc) ? $boolSortAsc : true);

        // Sort
        usort(
            $result,
            function($strKey1, $strKey2)
            {
                $objMigration1 = $this->getObjMigration($strKey1);
                $objMigration2 = $this->getObjMigration($strKey2);
                $result = $objMigration1->getIntSortCompare($objMigration2);

                return $result;
            }
        );

        // Get descending sort, if required
        if(!$boolSortAsc)
        {
            krsort($result);
            $result = array_values($result);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjMigration($strKey)
    {
        // Init var
        $result = null;

        // Try to get route object if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setMigration(MigrationInterface $objMigration)
	{
		// Init var
		$strKey = $objMigration->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objMigration);
		
		// return result
		return $strKey;
	}
	
	
	
	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register migration collection on migration object
        /** @var MigrationInterface $val */
		$val->setMigrationCollection($this);
    }



    /**
     * @inheritdoc
     */
    public function setTabMigration($tabMigration)
    {
        // Init var
        $result = array();

        // Case index array of migrations
        if(is_array($tabMigration))
        {
            // Run all migrations and for each, try to set
            foreach($tabMigration as $migration)
            {
                $strKey = $this->setMigration($migration);
                $result[] = $strKey;
            }
        }
        // Case collection of routes
        else if($tabMigration instanceof MigrationCollectionInterface)
        {
            // Run all migrations and for each, try to set
            foreach($tabMigration->getTabKey() as $strKey)
            {
                $objMigration = $tabMigration->getObjMigration($strKey);
                $strKey = $this->setMigration($objMigration);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeMigration($strKey)
    {
        // Init var
        $result = $this->getObjMigration($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeMigrationAll(array $tabConfig = null, $isExecuted = null)
    {
        // Ini var
        $tabKey = $this->getTabKey($tabConfig, $isExecuted);

        foreach($tabKey as $strKey)
        {
            $this->removeMigration($strKey);
        }
    }



}