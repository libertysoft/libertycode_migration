<?php
/**
 * Description :
 * This class allows to define default migration class.
 * Can be consider is base of all migration type.
 *
 * Default migration uses the following specified configuration:
 * [
 *     key(optional: got static class name if not found): "string migration key",
 *
 *     is_executed(optional: got false if not found): true / false
 * ]
 *
 * Default migration uses the following specified selection configuration, to be selected:
 * [
 *     regexp_key(optional): "string REGEXP pattern",
 *
 *     list_key(optional): [
 *         "string key 1",
 *         ...,
 *         "string key N"
 *     ],
 *
 *     sort_compare_key(optional): [
 *         // Condition 1
 *         [
 *             operator(optional: got @see ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL if operator not found):
 *                 "string operator" (@see ConstMigration::getTabGetConfigSortCompareOperator() ),
 *
 *             value(required): "string key"
 *         ],
 *         ...,
 *         // And condition N
 *         [...]
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\migration\migration\api\MigrationInterface;

use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migration\exception\ConfigInvalidFormatException;
use liberty_code\migration\migration\exception\MigrationCollectionInvalidFormatException;
use liberty_code\migration\migration\exception\GetConfigInvalidFormatException;
use liberty_code\migration\migration\exception\ExecuteEnableCallException;
use liberty_code\migration\migration\exception\RollbackEnableCallException;



abstract class DefaultMigration extends FixBean implements MigrationInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param MigrationCollectionInterface $objMigrationCollection = null
     */
    public function __construct(
        array $tabConfig = null,
        MigrationCollectionInterface $objMigrationCollection = null
    )
    {
        // Init var

        // Call parent constructor
        parent::__construct();

        // Init migration collection if required
        if(!is_null($objMigrationCollection))
        {
            $this->setMigrationCollection($objMigrationCollection);
        }

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION))
        {
            $this->__beanTabData[ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstMigration::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstMigration::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION,
            ConstMigration::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION:
                    MigrationCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstMigration::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsExecuted()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstMigration::TAB_CONFIG_KEY_IS_EXECUTED, $tabConfig) ?
                (intval($tabConfig[ConstMigration::TAB_CONFIG_KEY_IS_EXECUTED]) != 0) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws GetConfigInvalidFormatException
     */
    public function checkIsSelected(array $tabConfig)
    {
        // Set check arguments
        GetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $strKey = $this->getStrKey();
        $strGetRegexpKey = (
            array_key_exists(ConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY, $tabConfig) ?
                $tabConfig[ConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY] :
                null
        );
        $tabGetTabKey = (
            array_key_exists(ConstMigration::TAB_GET_CONFIG_KEY_LIST_KEY, $tabConfig) ?
                $tabConfig[ConstMigration::TAB_GET_CONFIG_KEY_LIST_KEY] :
                null
        );
        $tabGetSortCompareKey = (
            array_key_exists(ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY, $tabConfig) ?
                $tabConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY] :
                null
        );

        // Init sort comparison function
        $checkTabSortCompare = function($tabSortCompare, $getIntSortCompare)
        {
            $result = true;

            // Run each sub-configuration
            for($intCpt = 0; ($intCpt < count($tabSortCompare)) && $result; $intCpt++)
            {
                $subConfig = $tabSortCompare[$intCpt];
                $operator = (
                    isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR]) ?
                        $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR] :
                        ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL
                );
                $value = $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE];
                $intSortCompare = $getIntSortCompare($value);

                // Check order comparison
                switch($operator)
                {
                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_LESS);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS_EQUAL:
                        $result = (
                            ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL) ||
                            ($intSortCompare == ConstMigration::SORT_COMPARE_LESS)
                        );
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_GREATER);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER_EQUAL:
                        $result = (
                            ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL) ||
                            ($intSortCompare == ConstMigration::SORT_COMPARE_GREATER)
                        );
                        break;
                }
            }

            return $result;
        };

        // Check if migration selected
        $result =
            // Check key selection
            (
                (is_null($strGetRegexpKey)) ||
                (preg_match($strGetRegexpKey, $strKey) === 1)
            ) &&
            (
                (is_null($tabGetTabKey)) ||
                (in_array($strKey, $tabGetTabKey))
            ) &&
            (
                (is_null($tabGetSortCompareKey)) ||
                $checkTabSortCompare(
                    $tabGetSortCompareKey,
                    function($strValueKey)
                    {
                        return $this->getIntSortCompareKey($strValueKey);
                    }
                )
            );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjMigrationCollection()
    {
        // Return result
        return $this->beanGet(ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstMigration::DATA_KEY_DEFAULT_CONFIG);
    }
	
	

    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstMigration::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstMigration::TAB_CONFIG_KEY_KEY] :
                ToolBoxClassReflection::getStrClassName(static::class)
        );

        // Return result
        return $result;
    }



    /**
     * Get sort comparison analysis,
     * from the specified migration key.
     * Result format:
     * @see MigrationInterface::getIntSortCompare()
     *
     * @param string $strMigrationKey
     * @return integer
     */
    protected function getIntSortCompareKey($strMigrationKey)
    {
        // Init var
        $strKey = $this->getStrKey();

        // Get alphanumeric key comparison
        $result = (
            ($strKey > $strMigrationKey) ?
                ConstMigration::SORT_COMPARE_GREATER :
                (
                    ($strKey < $strMigrationKey) ?
                        ConstMigration::SORT_COMPARE_LESS :
                        ConstMigration::SORT_COMPARE_EQUAL
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntSortCompare(MigrationInterface $objMigration)
    {
        // Init var
        $strMigrationKey = $objMigration->getStrKey();
        $result = $this->getIntSortCompareKey($strMigrationKey);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setMigrationCollection(MigrationCollectionInterface $objMigrationCollection = null)
	{
		$this->beanSet(ConstMigration::DATA_KEY_DEFAULT_MIGRATION_COLLECTION, $objMigrationCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstMigration::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setIsExecuted($boolIsExecuted)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set force access option
        $tabConfig[ConstMigration::TAB_CONFIG_KEY_IS_EXECUTED] = $boolIsExecuted;

        // Set configuration
        $this->setConfig($tabConfig);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute migration.
     */
    abstract protected function executeMigration();



    /**
     * @inheritdoc
     * @throws ExecuteEnableCallException
     */
    public function execute($boolForce = false)
    {
        // Execute migration, if required
        if((!$this->checkIsExecuted()) || $boolForce)
        {
            $this->executeMigration();
            $this->setIsExecuted(true);
        }
        else
        {
            throw new ExecuteEnableCallException($this->getStrKey());
        }
    }



    /**
     * Rollback migration.
     */
    abstract protected function rollbackMigration();



    /**
     * @inheritdoc
     * @throws RollbackEnableCallException
     */
    public function rollback($boolForce = false)
    {
        // Rollback migration, if required
        if($this->checkIsExecuted() || $boolForce)
        {
            $this->rollbackMigration();
            $this->setIsExecuted(false);
        }
        else
        {
            throw new RollbackEnableCallException($this->getStrKey());
        }
    }



}