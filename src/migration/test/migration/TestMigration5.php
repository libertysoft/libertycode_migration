<?php

namespace liberty_code\migration\migration\test\migration;

use liberty_code\migration\migration\test\TestMigration;

//use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\version\library\ConstVersionMigration;

class TestMigration5 extends TestMigration
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            //ConstMigration::TAB_CONFIG_KEY_KEY => '',
            //ConstMigration::TAB_CONFIG_KEY_IS_EXECUTED => false,
            ConstVersionMigration::TAB_CONFIG_KEY_VERSION => '1.0.1',
            ConstVersionMigration::TAB_CONFIG_KEY_ORDER => 1,
            //ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT => ConstMigration::SORT_COMPARE_EQUAL,
            ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY => true
        );
    }



}