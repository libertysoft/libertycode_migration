<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/migration/test/TestMigration.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration1.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration2.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration3.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration4.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration5.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration6.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration7.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration8.php');
require_once($strRootAppPath . '/src/migration/test/migration/TestMigration9.php');

// Use
use liberty_code\migration\migration\model\DefaultMigrationCollection;

// Use test
use liberty_code\migration\migration\test\migration\TestMigration1;
use liberty_code\migration\migration\test\migration\TestMigration2;
use liberty_code\migration\migration\test\migration\TestMigration3;
use liberty_code\migration\migration\test\migration\TestMigration4;
use liberty_code\migration\migration\test\migration\TestMigration5;
use liberty_code\migration\migration\test\migration\TestMigration6;
use liberty_code\migration\migration\test\migration\TestMigration7;
use liberty_code\migration\migration\test\migration\TestMigration8;
use liberty_code\migration\migration\test\migration\TestMigration9;



// Init var
$objMigration1 = new TestMigration1();
$objMigration2 = new TestMigration2();
$objMigration3 = new TestMigration3();
$objMigration4 = new TestMigration4();
$objMigration5 = new TestMigration5();
$objMigration6 = new TestMigration6();
$objMigration7 = new TestMigration7();
$objMigration8 = new TestMigration8();
$objMigration9 = new TestMigration9();
$tabMigration = array(
    $objMigration9,
    $objMigration8,
    $objMigration7,
    $objMigration6,
    $objMigration5,
    $objMigration4,
    $objMigration3,
    $objMigration2,
    $objMigration1
);

$objMigrationCollection = new DefaultMigrationCollection();
$objMigrationCollection->setTabMigration($tabMigration);



// Test migration execute
echo('Test migration execute: <br />');
$objMigration = $objMigrationCollection->getObjMigration($objMigration1->getStrKey());

echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Execute: <pre>');$objMigration->execute();echo('</pre>');
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');

try{
    echo('Execute: <pre>');$objMigration->execute();echo('</pre>');
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

$objMigration->setIsExecuted(false);
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Execute: <pre>');$objMigration->execute();echo('</pre>');

echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Execute: <pre>');$objMigration->execute(true);echo('</pre>');
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');

echo('<br /><br /><br />');



// Test migration rollback
echo('Test migration rollback: <br />');

echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Rollback: <pre>');$objMigration->rollback();echo('</pre>');
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');

try{
    echo('Rollback: <pre>');$objMigration->rollback();echo('</pre>');
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

$objMigration->setIsExecuted(true);
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Rollback: <pre>');$objMigration->rollback();echo('</pre>');

echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
echo('Rollback: <pre>');$objMigration->rollback(true);echo('</pre>');
echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from key
$tabKey = $objMigrationCollection->getTabKey();
foreach($tabKey as $strKey)
{
    echo('Test check, get key "'.$strKey.'": <br />');
    try{
        $objMigration = $objMigrationCollection->getObjMigration($strKey);
        $boolMigrationExists = $objMigrationCollection->checkExists($strKey);

        echo('Check: <pre>');var_dump($boolMigrationExists);echo('</pre>');

        if(!is_null($objMigration))
        {
            echo('Get: class: <pre>');print_r(get_class($objMigration));echo('</pre>');
            echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
            echo('Get: key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
            echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
            echo('Get: migration collection count: <pre>');
                print_r(count($objMigration->getObjMigrationCollection()->getTabKey()));
            echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test check/get from ASC sorted key
$tabKey = $objMigrationCollection->getTabSortKey();
foreach($tabKey as $strKey)
{
    echo('Test check, get ASC sorted key "'.$strKey.'": <br />');
    try{
        $objMigration = $objMigrationCollection->getObjMigration($strKey);
        $boolMigrationExists = $objMigrationCollection->checkExists($strKey);

        echo('Check: <pre>');var_dump($boolMigrationExists);echo('</pre>');

        if(!is_null($objMigration))
        {
            echo('Get: class: <pre>');print_r(get_class($objMigration));echo('</pre>');
            echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
            echo('Get: key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
            echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
            echo('Get: migration collection count: <pre>');
            print_r(count($objMigration->getObjMigrationCollection()->getTabKey()));
            echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test check/get from DESC sorted key
$tabKey = $objMigrationCollection->getTabSortKey(null, false);
foreach($tabKey as $strKey)
{
    echo('Test check, get DESC sorted key "'.$strKey.'": <br />');
    try{
        $objMigration = $objMigrationCollection->getObjMigration($strKey);
        $boolMigrationExists = $objMigrationCollection->checkExists($strKey);

        echo('Check: <pre>');var_dump($boolMigrationExists);echo('</pre>');

        if(!is_null($objMigration))
        {
            echo('Get: class: <pre>');print_r(get_class($objMigration));echo('</pre>');
            echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
            echo('Get: key: <pre>');var_dump($objMigration->getStrKey());echo('</pre>');
            echo('Check: is executed: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
            echo('Get: migration collection count: <pre>');
            print_r(count($objMigration->getObjMigrationCollection()->getTabKey()));
            echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test selection configuration
$tabConfig = array(
    // Ok: Found (2, 3)
    [
        'regexp_key' => '#TestMigration[1-3]$#'
    ],

    // Ko: Bad format
    [
        'regexp_key' => 7
    ],

    // Ok: Found
    [
        'list_key' => ['TestMigration5', 'TestMigration6', 'TestMigration7']
    ],

    // Ko: Not found
    [
        'list_key' => ['TestMigration10', 'TestMigration11']
    ],

    // Ok: Found (2, 3, 4, 5, 6, 7)
    [
        'sort_compare_key' => [
            [
                'operator' => 'greater_equal',
                'value' => 'TestMigration2'
            ],
            [
                'operator' => 'less_equal',
                'value' => 'TestMigration7'
            ]
        ]
    ],

    // Ok: Found (1, 2, 3, 4, 5, 6)
    [
        'regexp_version' => '#1\.0\.\d+#'
    ],

    // Ko: Not found
    [
        'list_version' => ['1.0.5', '1.0.7']
    ],

    // Ok: Found (1, 2, 3, 7, 8, 9)
    [
        'list_version' => ['1.0.0', '1.2.0']
    ],

    // Ok: Found (2)
    [
        'regexp_version' => '#1\.0\.\d+#',
        'regexp_order' => '#2#'
    ],

    // Ok: Found (1, 3, 6)
    [
        'regexp_version' => '#1\.0\.\d+#',
        'list_order' => [0, 7]
    ],

    // Ko: Bad format
    [
        'sort_compare_version' => [
            [
                'operator' => 'greaters',
                'value' => '1.0.0'
            ],
            [
                'operator' => 'lesss',
                'value' => '1.2'
            ]
        ]
    ],

    // Ok: Found (4, 5, 6)
    [
        'sort_compare_version' => [
            [
                'operator' => 'greater',
                'value' => '1.0.0'
            ],
            [
                'operator' => 'less',
                'value' => '1.2'
            ]
        ]
    ],

    // Ko: Not found
    [
        'sort_compare_order' => [
            [
                'operator' => 'greater',
                'value' => 20
            ]
        ]
    ],

    // Ok: Found (4, 6)
    [
        'sort_compare_version' => [
            [
                'operator' => 'greater',
                'value' => '1.0.0'
            ],
            [
                'operator' => 'less',
                'value' => '1.2'
            ]
        ],
        'sort_compare_order' => [
            [
                'operator' => 'greater',
                'value' => 1
            ],
            [
                'operator' => 'less',
                'value' => 20
            ]
        ]
    ],
);

foreach($tabConfig as $config)
{
    echo('Test selection configuration: <br />');
    echo('Config: <pre>');var_dump($config);echo('</pre>');

    try{
        $tabKey = $objMigrationCollection->getTabKey($config);
        $tabSortKeyAsc = $objMigrationCollection->getTabSortKey($config, true);
        $tabSortKeyDesc = $objMigrationCollection->getTabSortKey($config, false);

        echo('Get array of keys: <pre>');var_dump($tabKey);echo('</pre>');
        echo('Get array of ASC sorted keys: <pre>');var_dump($tabSortKeyAsc);echo('</pre>');
        echo('Get array of DESC sorted keys: <pre>');var_dump($tabSortKeyDesc);echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test selection is executed configuration
$objMigration1->execute();
$objMigration3->execute();
$objMigration5->execute();
$objMigration7->execute();
$objMigration9->execute();

$tabKey = $objMigrationCollection->getTabKey(null, true);
$tabSortKeyAsc = $objMigrationCollection->getTabSortKey(null, true, true);
$tabSortKeyDesc = $objMigrationCollection->getTabSortKey(null, false, true);

echo('Executed migration: Get array of keys: <pre>');var_dump($tabKey);echo('</pre>');
echo('Executed migration: Get array of ASC sorted keys: <pre>');var_dump($tabSortKeyAsc);echo('</pre>');
echo('Executed migration: Get array of DESC sorted keys: <pre>');var_dump($tabSortKeyDesc);echo('</pre>');

$tabKey = $objMigrationCollection->getTabKey(null, false);
$tabSortKeyAsc = $objMigrationCollection->getTabSortKey(null, true, false);
$tabSortKeyDesc = $objMigrationCollection->getTabSortKey(null, false, false);

echo('Not executed migration: Get array of keys: <pre>');var_dump($tabKey);echo('</pre>');
echo('Not executed migration: Get array of ASC sorted keys: <pre>');var_dump($tabSortKeyAsc);echo('</pre>');
echo('Not executed migration: Get array of DESC sorted keys: <pre>');var_dump($tabSortKeyDesc);echo('</pre>');

echo('<br /><br /><br />');


