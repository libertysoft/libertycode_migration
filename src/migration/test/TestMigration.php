<?php

namespace liberty_code\migration\migration\test;

//use liberty_code\migration\migration\version\model\VersionMigration;
use liberty_code\migration\migration\version\fix\model\FixVersionMigration;

abstract class TestMigration extends FixVersionMigration
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function executeMigration()
    {
        echo('<br />Execute ' . $this->getStrKey() . '<br />');
    }



    /**
     * @inheritdoc
     */
    protected function rollbackMigration()
    {
        echo('<br />Rollback ' . $this->getStrKey() . '<br />');
    }



}