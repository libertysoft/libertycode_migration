<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\version\exception;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\version\library\ConstVersionMigration;



class GetConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstVersionMigration::EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = is_string($strValue) && (trim($strValue) != '');
            }

            return $result;
        };

        // Init integer index array check function
        $checkTabIntIsValid = function($tabInt)
        {
            $result = is_array($tabInt) && (count($tabInt) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabInt)) && $result; $intCpt++)
            {
                $intValue = $tabInt[$intCpt];
                $result =
                    (
                        is_int($intValue) ||
                        ctype_digit($intValue)
                    ) &&
                    (intval($intValue) >= 0);
            }

            return $result;
        };

        // Init sort comparison index array check function
        $checkTabSortCompareIsValid = function($tabSortCompare, $boolValueIsInt)
        {
            $result = is_array($tabSortCompare) && (count($tabSortCompare) > 0);

            // Run each sub-configuration
            $tabSortCompare = array_values($tabSortCompare);
            for($intCpt = 0; ($intCpt < count($tabSortCompare)) && $result; $intCpt++)
            {
                $subConfig = $tabSortCompare[$intCpt];
                $result =
                    // Check valid operator
                    (
                        (!isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR])) ||
                        (
                            is_string($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR]) &&
                            in_array(
                                $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR],
                                ConstMigration::getTabGetConfigSortCompareOperator()
                            )
                        )
                    ) &&

                    // Check valid value
                    isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) &&
                    (
                        (
                            $boolValueIsInt &&
                            (
                                is_int($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) ||
                                (
                                    is_string($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) &&
                                    ctype_digit($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE])
                                )
                            ) &&
                            (intval($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) >= 0)
                        ) ||
                        (
                            is_string($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) &&
                            (trim($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE]) != '')
                        )
                    );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid regexp version
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION])) ||
                (
                    is_string($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION]) &&
                    (trim($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION]) != '')
                )
            ) &&

            // Check valid list version
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_VERSION])) ||
                $checkTabStrIsValid($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_VERSION])
            ) &&

            // Check valid sort comparison on version
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION])) ||
                $checkTabSortCompareIsValid(
                    $config[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION],
                    false
                )
            ) &&

            // Check valid regexp order
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER])) ||
                (
                    is_string($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER]) &&
                    (trim($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER]) != '')
                )
            ) &&

            // Check valid list order
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_ORDER])) ||
                $checkTabIntIsValid($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_ORDER])
            ) &&

            // Check valid sort comparison on order
            (
                (!isset($config[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER])) ||
                $checkTabSortCompareIsValid(
                    $config[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER],
                    true
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&

                // Check valid config
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}