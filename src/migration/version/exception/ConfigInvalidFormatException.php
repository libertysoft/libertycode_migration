<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\version\exception;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\version\library\ConstVersionMigration;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstVersionMigration::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid version
            (
                (!isset($config[ConstVersionMigration::TAB_CONFIG_KEY_VERSION])) ||
                (
                    is_string($config[ConstVersionMigration::TAB_CONFIG_KEY_VERSION]) &&
                    (trim($config[ConstVersionMigration::TAB_CONFIG_KEY_VERSION]) != '')
                )
            );

            // Check valid order
            (
                (!isset($config[ConstVersionMigration::TAB_CONFIG_KEY_ORDER])) ||
                (
                    is_int($config[ConstVersionMigration::TAB_CONFIG_KEY_ORDER]) &&
                    ($config[ConstVersionMigration::TAB_CONFIG_KEY_ORDER] >= 0)
                )
            ) &&

            // Check valid default sort comparison analysis
            (
                (!isset($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT])) ||
                (
                    is_int($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT]) &&
                    in_array(
                        $config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT],
                        ConstMigration::getTabSortCompare()
                    )
                )
            ) &&

            // Check valid sort comparison by key option
            (
                (!isset($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    is_int($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    (
                        is_string($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) &&
                        ctype_digit($config[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}