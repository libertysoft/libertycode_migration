<?php
/**
 * Description :
 * This class allows to define version migration class.
 * Version migration uses standard versions and order positions,
 * to sort migrations.
 *
 * Version migration uses the following specified configuration:
 * [
 *     Default migration configuration,
 *
 *     version(optional: '1.0.0' got if not found): "string standard version number",
 *
 *     order(optional: 0 got if not found): positive integer,
 *
 *     sort_compare_default(optional: got @see ConstMigration::SORT_COMPARE_EQUAL if not found):
 *         integer sort comparison analysis (@see MigrationInterface::getIntSortCompare() ),
 *
 *     sort_compare_use_key(optional: got false if not found): true / false
 *         Use alphanumeric key sort comparison if true
 * ]
 *
 * Version migration uses the following specified selection configuration, to be selected:
 * [
 *     Default migration selection configuration,
 *
 *     regexp_version(optional): "string REGEXP pattern",
 *
 *     list_version(optional): [
 *         "string version 1",
 *         ...,
 *         "string version N"
 *     ],
 *
 *     sort_compare_version(optional): [
 *         // Condition 1
 *         [
 *             operator(optional: got @see ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL if operator not found):
 *                 "string operator" (@see ConstMigration::getTabGetConfigSortCompareOperator() ),
 *
 *             value(required): "string version"
 *         ],
 *         ...,
 *         // And condition N
 *         [...]
 *     ],
 *
 *     regexp_order(optional): "string REGEXP pattern",
 *
 *     list_order(optional): [
 *         positive integer order 1,
 *         ...,
 *         positive integer order N
 *     ],
 *
 *     sort_compare_order(optional): [
 *         // Condition 1
 *         [
 *             operator(optional: got @see ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL if operator not found):
 *                 "string operator" (@see ConstMigration::getTabGetConfigSortCompareOperator() ),
 *
 *             value(required): positive integer order
 *         ],
 *         ...,
 *         // And condition N
 *         [...]
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\version\model;

use liberty_code\migration\migration\model\DefaultMigration;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\version\library\ConstVersionMigration;
use liberty_code\migration\migration\version\exception\ConfigInvalidFormatException;
use liberty_code\migration\migration\version\exception\GetConfigInvalidFormatException;



abstract class VersionMigration extends DefaultMigration
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMigration::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if sort comparison by key option is selected.
     *
     * @return boolean
     */
    protected function checkSortCompareUseKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY, $tabConfig) ?
                (intval($tabConfig[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) != 0) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws GetConfigInvalidFormatException
     */
    public function checkIsSelected(array $tabConfig)
    {
        // Set check arguments
        GetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $strVersion = $this->getStrVersion();
        $intOrder = $this->getIntOrder();
        $strGetRegexpVersion = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION] :
                null
        );
        $tabGetTabVersion = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_VERSION, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_VERSION] :
                null
        );
        $tabGetSortCompareVersion = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION] :
                null
        );
        $strGetRegexpOrder = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER] :
                null
        );
        $tabGetTabOrder = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_ORDER, $tabConfig) ?
                array_map(
                    function($order) {return intval($order);},
                    $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_ORDER]
                ) :
                null
        );
        $tabGetSortCompareOrder = (
            array_key_exists(ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER] :
                null
        );

        // Init sort comparison function
        $checkTabSortCompare = function($tabSortCompare, $getIntSortCompare)
        {
            $result = true;

            // Run each sub-configuration
            for($intCpt = 0; ($intCpt < count($tabSortCompare)) && $result; $intCpt++)
            {
                $subConfig = $tabSortCompare[$intCpt];
                $operator = (
                    isset($subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR]) ?
                        $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR] :
                        ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL
                );
                $value = $subConfig[ConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE];
                $intSortCompare = $getIntSortCompare($value);

                // Check sort comparison
                switch($operator)
                {
                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_EQUAL:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_LESS);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_LESS_EQUAL:
                        $result = (
                            ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL) ||
                            ($intSortCompare == ConstMigration::SORT_COMPARE_LESS)
                        );
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER:
                        $result = ($intSortCompare == ConstMigration::SORT_COMPARE_GREATER);
                        break;

                    case ConstMigration::GET_CONFIG_SORT_COMPARE_OPERATOR_GREATER_EQUAL:
                        $result = (
                            ($intSortCompare == ConstMigration::SORT_COMPARE_EQUAL) ||
                            ($intSortCompare == ConstMigration::SORT_COMPARE_GREATER)
                        );
                        break;
                }
            }

            return $result;
        };

        // Check if migration selected
        $result =
            // Check parent selection
            parent::checkIsSelected($tabConfig) &&

            // Check version selection
            (
                (is_null($strGetRegexpVersion)) ||
                (preg_match($strGetRegexpVersion, $strVersion) === 1)
            ) &&
            (
                (is_null($tabGetTabVersion)) ||
                (in_array($strVersion, $tabGetTabVersion))
            ) &&
            (
                (is_null($tabGetSortCompareVersion)) ||
                $checkTabSortCompare(
                    $tabGetSortCompareVersion,
                    function($strValueVersion)
                    {
                        return $this->getIntSortCompareVersion($strValueVersion);
                    }
                )
            ) &&

            // Check order selection
            (
                (is_null($strGetRegexpOrder)) ||
                (preg_match($strGetRegexpOrder, strval($intOrder)) === 1)
            ) &&
            (
                (is_null($tabGetTabOrder)) ||
                (in_array($intOrder, $tabGetTabOrder))
            ) &&
            (
                (is_null($tabGetSortCompareOrder)) ||
                $checkTabSortCompare(
                    $tabGetSortCompareOrder,
                    function($intValueOrder)
                    {
                        $intValueOrder = intval($intValueOrder);
                        return $this->getIntSortCompareOrder($intValueOrder);
                    }
                )
            );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string version.
     *
     * @return string
     */
    public function getStrVersion()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstVersionMigration::TAB_CONFIG_KEY_VERSION, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_CONFIG_KEY_VERSION] :
                '1.0.0'
        );

        // Return result
        return $result;
    }



    /**
     * Get integer order.
     *
     * @return integer
     */
    public function getIntOrder()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstVersionMigration::TAB_CONFIG_KEY_ORDER, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_CONFIG_KEY_ORDER] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * Get default sort comparison analysis,
     * from configuration.
     *
     * @return integer
     */
    protected function getIntSortCompareDefault()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT, $tabConfig) ?
                $tabConfig[ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT] :
                ConstMigration::SORT_COMPARE_EQUAL
        );

        // Return result
        return $result;
    }



    /**
     * Get sort comparison analysis,
     * from the specified version migration version.
     * Result format:
     * @see MigrationInterface::getIntSortCompare()
     *
     * @param string $strMigrationVersion
     * @return integer
     */
    protected function getIntSortCompareVersion($strMigrationVersion)
    {
        // Init var
        $result = $this->getIntSortCompareDefault();
        $strVersion = $this->getStrVersion();

        // Get version comparison
        $intSortCompare = version_compare(
            $strVersion,
            $strMigrationVersion
        );
        $intSortCompare = (
            is_numeric($intSortCompare) ?
                (
                    (intval($intSortCompare) > 0) ?
                        ConstMigration::SORT_COMPARE_GREATER :
                        (
                            (intval($intSortCompare) < 0) ?
                                ConstMigration::SORT_COMPARE_LESS :
                                ConstMigration::SORT_COMPARE_EQUAL
                        )
                ) :
                $intSortCompare
        );

        // Register sort comparison in result, if required
        if(in_array($intSortCompare, ConstMigration::getTabSortCompare()))
        {
            $result = $intSortCompare;
        }

        // Return result
        return $result;
    }



    /**
     * Get sort comparison analysis,
     * from the specified version migration order.
     * Result format:
     * @see MigrationInterface::getIntSortCompare()
     *
     * @param integer $intMigrationOrder
     * @return integer
     */
    protected function getIntSortCompareOrder($intMigrationOrder)
    {
        // Init var
        $intOrder = $this->getIntOrder();

        // Get order comparison
        $result = (
            ($intOrder > $intMigrationOrder) ?
                ConstMigration::SORT_COMPARE_GREATER :
                (
                    ($intOrder < $intMigrationOrder) ?
                        ConstMigration::SORT_COMPARE_LESS :
                        ConstMigration::SORT_COMPARE_EQUAL
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get sort comparison analysis engine,
     * from the specified version migration.
     * Result format:
     * @see MigrationInterface::getIntSortCompare()
     *
     * @param $objMigration
     * @return integer
     */
    protected function getIntSortCompareEngine(VersionMigration $objMigration)
    {
        // Init var
        $strMigrationVersion = $objMigration->getStrVersion();

        // Get version comparison
        $result = $this->getIntSortCompareVersion($strMigrationVersion);

        // Get order comparison, if required
        if($result == ConstMigration::SORT_COMPARE_EQUAL)
        {
            $intMigrationOrder = $objMigration->getIntOrder();
            $result = $this->getIntSortCompareOrder($intMigrationOrder);
        }

        // Get alphanumeric key comparison, if required
        if(
            ($result == ConstMigration::SORT_COMPARE_EQUAL) &&
            $this->checkSortCompareUseKey()
        )
        {
            $strMigrationKey = $objMigration->getStrKey();
            $result = $this->getIntSortCompareKey($strMigrationKey);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntSortCompare(MigrationInterface $objMigration)
    {
        // Init var
        $result = parent::getIntSortCompare($objMigration);

        // Get sort comparison, if possible
        if($objMigration instanceof VersionMigration)
        {
            $result = $this->getIntSortCompareEngine($objMigration);
        }

        // Return result
        return $result;
    }



}