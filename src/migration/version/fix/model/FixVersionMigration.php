<?php
/**
 * Description :
 * This class allows to define fixed version migration class.
 * Fixed version migration allows to execute migration and rollback it,
 * from a specified fixed configuration.
 *
 * Fixed version migration uses the following specified configuration:
 * [
 *     Version migration configuration
 * ]
 *
 * Fixed version migration uses the following specified selection configuration, to be selected:
 * [
 *     Version migration selection configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\version\fix\model;

use liberty_code\migration\migration\version\model\VersionMigration;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migration\version\fix\exception\ConfigInvalidFormatException;



abstract class FixVersionMigration extends VersionMigration
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(MigrationCollectionInterface $objMigrationCollection = null
    )
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($tabConfig, $objMigrationCollection);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMigration::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}