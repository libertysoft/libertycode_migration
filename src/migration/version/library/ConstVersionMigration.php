<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration\migration\version\library;



class ConstVersionMigration
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_VERSION = 'version';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT = 'sort_compare_default';
    const TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY = 'sort_compare_use_key';

    // Configuration get
    const TAB_GET_CONFIG_KEY_REGEXP_VERSION = 'regexp_version';
    const TAB_GET_CONFIG_KEY_LIST_VERSION = 'list_version';
    const TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION = 'sort_compare_version';
    const TAB_GET_CONFIG_KEY_REGEXP_ORDER = 'regexp_order';
    const TAB_GET_CONFIG_KEY_LIST_ORDER = 'list_order';
    const TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER = 'sort_compare_order';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the version migration configuration standard.';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the version migration get configuration standard.';
}